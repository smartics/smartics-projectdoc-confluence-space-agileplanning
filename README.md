projectdoc Doctypes for Agile Planning
======================================

Deprecated: New version of this project is found at https://bitbucket.org/smartics/smartics-doctype-model-agileplanning

##Overview

This is a free add-on for [projectdoc](https://www.smartics.eu/confluence/display/PDAC1/) for Confluence.

It provides the blueprints to create pages for

  * Iterations, Retrospectives, Reviews
  * User Stories
  * Announcements
  * Product Backlogs, Improvement Backlogs

It also provides a space blueprint to get started with your software documentation project quickly.

##Fork me!
Feel free to fork this project to adjust the templates according to your project requirements.

The projectdoc Core Doctypes Add-on is licensed under [Apache License Version 2.0](http://www.apache.org/licenses/LICENSE-2.0)

##Documentation

For more information please visit

  * the [add-on's homepage](https://www.smartics.eu/confluence/display/PDAC1/Doctypes+for+Agile+Planning)
  * the [add-on on the Atlassian Marketplace](https://marketplace.atlassian.com/plugins/de.smartics.atlassian.confluence.smartics-projectdoc-confluence-space-agileplanning)
