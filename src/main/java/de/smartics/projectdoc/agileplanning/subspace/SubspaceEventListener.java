/*
 * Copyright 2014-2018 smartics, Kronseder & Reiner GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.smartics.projectdoc.agileplanning.subspace;

import de.smartics.projectdoc.atlassian.confluence.page.partition.AbstractCreatePagesEventListener;
import de.smartics.projectdoc.atlassian.confluence.persistence.ProjectdocMarkerSupport;

import com.atlassian.confluence.languages.LocaleManager;
import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.pages.PageManager;
import com.atlassian.confluence.plugins.createcontent.ContentBlueprintManager;
import com.atlassian.confluence.plugins.createcontent.api.events.BlueprintPageCreateEvent;
import com.atlassian.confluence.plugins.createcontent.api.services.ContentBlueprintService;
import com.atlassian.confluence.util.i18n.I18NBean;
import com.atlassian.confluence.util.i18n.I18NBeanFactory;
import com.atlassian.event.api.EventListener;
import com.atlassian.plugin.ModuleCompleteKey;

import java.util.HashSet;
import java.util.Set;

/**
 * Adjusts documents on creation for the homepage of a subspace.
 */
public class SubspaceEventListener extends AbstractCreatePagesEventListener {
  // ********************************* Fields *********************************

  // --- constants ------------------------------------------------------------

  /**
   * The module key for the main home template.
   */
  private static final String MODULE_KEY =
      "de.smartics.atlassian.confluence.smartics-projectdoc-confluence-space-agileplanning:projectdoc-subspace-agileplanning-main-blueprint";

  /**
   * The key to the blueprint that summarizes all subordinate blueprints.
   */
  public static final String BLUEPRINT_KEY =
      "de.smartics.atlassian.confluence.smartics-projectdoc-confluence-space-agileplanning:projectdoc-subspace-agileplanning-main-doctypehome-blueprint";

  private static final ModuleCompleteKey MODULE_COMPLETE_KEY =
      new ModuleCompleteKey(BLUEPRINT_KEY);

  // --- members --------------------------------------------------------------

  // ****************************** Initializer *******************************

  // ****************************** Constructors ******************************

  /**
   * Default constructor.
   */
  public SubspaceEventListener() {}

  // ****************************** Inner Classes *****************************

  // ********************************* Methods ********************************

  // --- init -----------------------------------------------------------------

  // --- get&set --------------------------------------------------------------

  @Override
  public void setPageManager(final PageManager pageManager) {
    super.setPageManager(pageManager);
  }

  @Override
  public void setContentBlueprintManager(
      final ContentBlueprintManager contentBlueprintManager) {
    super.setContentBlueprintManager(contentBlueprintManager);
  }

  @Override
  public void setContentBlueprintService(
      final ContentBlueprintService contentBlueprintService) {
    super.setContentBlueprintService(contentBlueprintService);
  }

  @Override
  public void setLocaleManager(final LocaleManager localeManager) {
    super.setLocaleManager(localeManager);
  }

  @Override
  public void setI18nBeanFactory(final I18NBeanFactory i18nBeanFactory) {
    super.setI18nBeanFactory(i18nBeanFactory);
  }

  @Override
  public void setProjectdocMarkerSupport(
      final ProjectdocMarkerSupport contentPropertySupport) {
    super.setProjectdocMarkerSupport(contentPropertySupport);
  }

  @Override
  protected String getModuleKey() {
    return MODULE_KEY;
  }

  @Override
  protected String getBlueprintKey() {
    return BLUEPRINT_KEY;
  }

  @Override
  protected ModuleCompleteKey getModuleCompleteKey() {
    return MODULE_COMPLETE_KEY;
  }

  // --- business -------------------------------------------------------------

  @EventListener
  public void onBlueprintCreateEvent(final BlueprintPageCreateEvent event) {
    super.onBlueprintCreateEvent(event);
  }

  private static final Set<String> TYPE_TEMPLATE_SHORT_KEYS;

  static {
    final Set<String> set = new HashSet<String>();
    set.add("experience-level-home-template");
    set.add("module-type-home-template");
    set.add("resource-type-home-template");
    set.add("topic-type-home-template");
    TYPE_TEMPLATE_SHORT_KEYS = set;
  }

  private static final Set<String> BACKLOG_TEMPLATE_SHORT_KEYS;

  static {
    final Set<String> set = new HashSet<String>();
    set.add("announcement-home-template");
    set.add("improvement-home-template");
    set.add("product-backlog-home-template");
    BACKLOG_TEMPLATE_SHORT_KEYS = set;
  }

  protected Page calcParentPage(final I18NBean i18n,
      final Page subspaceHomepage, final String shortTemplateKey) {
    if (TYPE_TEMPLATE_SHORT_KEYS.contains(shortTemplateKey)) {
      final String typeHomepageTitle =
          i18n.getText("projectdoc.doctype.type-home-template.name");
      final Page parentPage = getPageManager()
          .getPage(subspaceHomepage.getSpaceKey(), typeHomepageTitle);
      if (parentPage != null) {
        return parentPage;
      }
    } else if (BACKLOG_TEMPLATE_SHORT_KEYS.contains(shortTemplateKey)) {
      final String typeHomepageTitle =
          i18n.getText("projectdoc.content.planning.backlogs");
      final Page parentPage = getPageManager()
          .getPage(subspaceHomepage.getSpaceKey(), typeHomepageTitle);
      if (parentPage != null) {
        return parentPage;
      }
    }
    return subspaceHomepage;
  }

  // --- object basics --------------------------------------------------------

}
