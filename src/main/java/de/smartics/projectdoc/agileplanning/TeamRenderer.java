/*
 * Copyright 2014-2018 smartics, Kronseder & Reiner GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.smartics.projectdoc.agileplanning;

import com.atlassian.confluence.plugins.createcontent.api.events.SpaceBlueprintCreateEvent;
import com.atlassian.confluence.security.SpacePermission;
import com.atlassian.confluence.security.SpacePermissionManager;
import com.atlassian.confluence.spaces.Space;
import com.atlassian.confluence.spaces.SpaceDescription;
import com.atlassian.confluence.user.ConfluenceUser;
import com.atlassian.confluence.user.UserAccessor;
import com.atlassian.renderer.v2.components.HtmlEscaper;

import org.apache.commons.lang3.StringUtils;

import java.util.Map;

/**
 * Renders a team in an HTML table.
 */
class TeamRenderer {
  // ********************************* Fields *********************************

  // --- constants ------------------------------------------------------------

  private static final int MAX_PER_ROW = 3;

  // --- members --------------------------------------------------------------

  private final UserAccessor userAccessor;

  private final SpacePermissionManager spacePermissionManager;

  // ****************************** Initializer *******************************

  // ****************************** Constructors ******************************

  TeamRenderer(final UserAccessor userAccessor,
      final SpacePermissionManager spacePermissionManager) {
    this.userAccessor = userAccessor;
    this.spacePermissionManager = spacePermissionManager;
  }

  // ****************************** Inner Classes *****************************

  // ********************************* Methods ********************************

  // --- init -----------------------------------------------------------------

  // --- get&set --------------------------------------------------------------

  // --- business -------------------------------------------------------------

  void addTeam(final SpaceBlueprintCreateEvent event,
      final Map<String, Object> context) {
    final Space space = event.getSpace();
    final String motto = (String) context.get("motto");
    if (StringUtils.isNotBlank(motto)) {
      final SpaceDescription description = space.getDescription();
      description.setBodyAsString(motto);
    }

    final String team = (String) context.get("team");
    final String[] members = team.split(",");
    final String teamTable = createTeamTable(space, members);
    context.put("team", teamTable);
  }

  private String createTeamTable(final Space space, final String[] members) {
    final int memberCount = members.length;
    if (memberCount <= 0) {
      return StringUtils.EMPTY;
    }

    final StringBuilder buffer = new StringBuilder(4096);
    buffer.append("<table class=\"team\"><tbody>\n");

    final int maxColumnCount = Math.min(memberCount, MAX_PER_ROW);
    int columnIndex = maxColumnCount;
    for (final String member : members) {
      final ConfluenceUser user = userAccessor.getUserByName(member.trim());
      if (user == null) {
        continue;
      }

      if (columnIndex == maxColumnCount) {
        buffer.append("<tr>");
      }

      addTeamMember(buffer, user);
      assignPermission(space, user);

      columnIndex--;
      if (columnIndex == 0) {
        buffer.append("</tr>\n");
        columnIndex = maxColumnCount;
      }
    }

    if (memberCount > MAX_PER_ROW && columnIndex < maxColumnCount) {
      while (columnIndex-- > 0) {
        buffer.append("<td></td>");
      }
      buffer.append("</tr>\n");
    }

    buffer.append("</tbody></table>\n");
    return buffer.toString();
  }

  private static void addTeamMember(final StringBuilder buffer,
      final ConfluenceUser user) {
    buffer.append("<td><p style=\"text-align: center;\">");
    buffer
        .append(
            "<ac:structured-macro ac:name=\"profile-picture\"><ac:parameter ac:name=\"User\"><ri:user ri:userkey=\"")
        .append(HtmlEscaper.escapeAll(user.getKey().getStringValue(), false))
        .append("\" /></ac:parameter></ac:structured-macro>");
    buffer
        .append(
            "</p><p style=\"text-align: center;\"><strong><a href=\"mailto:")
        .append(HtmlEscaper.escapeAll(user.getEmail(), false)).append("\">")
        .append(HtmlEscaper.escapeAll(user.getFullName(), false))
        .append("</a></strong>");
    buffer.append("</p></td>");
  }

  private void assignPermission(final Space space, final ConfluenceUser user) {
    for (final String permission : SpacePermission.GENERIC_SPACE_PERMISSIONS) {
      spacePermissionManager.savePermission(
          SpacePermission.createUserSpacePermission(permission, space, user));
    }
  }

  // --- object basics --------------------------------------------------------

}
