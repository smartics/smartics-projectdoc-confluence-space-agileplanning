/*
 * Copyright 2014-2018 smartics, Kronseder & Reiner GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
Confluence.Blueprint.setWizard(
		'de.smartics.atlassian.confluence.smartics-projectdoc-confluence-space-agileplanning:create-doctype-template-announcement',
		PROJECTDOC.standardWizard);

Confluence.Blueprint.setWizard(
		'de.smartics.atlassian.confluence.smartics-projectdoc-confluence-space-agileplanning:create-doctype-template-improvement',
		PROJECTDOC.standardWizard);

Confluence.Blueprint.setWizard(
		'de.smartics.atlassian.confluence.smartics-projectdoc-confluence-space-agileplanning:create-doctype-template-product-backlog',
		PROJECTDOC.standardWizard);

(function ($) {
    function bindFields(ev, state) {
        $('#projectdoc-duration-from').datepicker({
            dateFormat : "yy-mm-dd"
        });
        $('#projectdoc-duration-to').datepicker({
            dateFormat : "yy-mm-dd"
        });

        var title = state.wizardData.title;
        if(title) {
          $('#projectdoc\\.doctype\\.common\\.name').val(title);
          $('#projectdoc-duration-from').focus();
        }

        wizard.assembleSubmissionObject = PROJECTDOC.assemblePageCreationData;
    }

    Confluence.Blueprint.setWizard('de.smartics.atlassian.confluence.smartics-projectdoc-confluence-space-agileplanning:create-doctype-template-iteration',
    		function(wizard) {
		wizard.on('pre-render.page1Id', function(e, state) {
			state.wizardData.selectedWebItemData = PROJECTDOC.prerenderWizard(e, state);
		});

		wizard.on("post-render.page1Id", bindFields);

        wizard.on('submit.page1Id', function(e, state) {
            var name = state.pageData["projectdoc.doctype.common.name"];
            if (!name) {
                alert('Please provide a name for the iteration.');
                return false;
            }

            var specifiedStartDate = state.pageData["projectdoc-duration-from"];
            if (!specifiedStartDate){
                alert('Please provide a start date for the iteration.');
                return false;
            }

            var specifiedEndDate = state.pageData["projectdoc-duration-to"];
            if (!specifiedEndDate){
                alert('Please provide an end date for the iteration.');
                return false;
            }

            var shortDescription = state.pageData["projectdoc.doctype.common.shortDescription"];
            if (!shortDescription){
                alert('Please provide a short description for the iteration.');
                return false;
            }

	        PROJECTDOC.adjustToLocation(state);
        });
    });
})(AJS.$);


Confluence.Blueprint.setWizard(
		'de.smartics.atlassian.confluence.smartics-projectdoc-confluence-space-agileplanning:create-doctype-template-retrospective',
		PROJECTDOC.standardWizard);

Confluence.Blueprint.setWizard(
		'de.smartics.atlassian.confluence.smartics-projectdoc-confluence-space-agileplanning:create-doctype-template-review',
		PROJECTDOC.standardWizard);

Confluence.Blueprint.setWizard(
		'de.smartics.atlassian.confluence.smartics-projectdoc-confluence-space-agileplanning:create-doctype-template-review-act',
		PROJECTDOC.standardWizard);

Confluence.Blueprint.setWizard(
		'de.smartics.atlassian.confluence.smartics-projectdoc-confluence-space-agileplanning:create-doctype-template-user-story',
		PROJECTDOC.standardWizard);
